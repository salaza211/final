
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String requester = request.getParameter("requester");
        String password = request.getParameter("password");
        String product = request.getParameter("product");
        int amount = Integer.parseInt(request.getParameter("amount"));

        CheckUser check = new CheckUser();
        boolean auth = check.CheckUser(requester, password);
        if (auth == false) {
            out.println("Wrong username and password combination, please try again.");
        }

        checkAccess check1 = new checkAccess();
        boolean auth1 = check1.CheckAccess(requester);
        if (auth1 == false & auth == true) {
            out.println("You do not have access to order, please request access.");
        }

        checkInventory check3 = new checkInventory();
        boolean inventory_check = check3.CheckInventory(product);

        if (inventory_check == false & auth1 == true & auth == true) {
            out.println("There is no product with that name. ");
        }

        checkAmount check4 = new checkAmount();
        boolean amount_check = check4.CheckAmount(amount, product);

        if (amount_check == false & inventory_check == true & auth1 == true & auth == true) {
            out.println("There is not enough of the product selected. ");
        }


        if (auth == true & auth1 == true & inventory_check == true & amount_check == true) {
            Orders order = new Orders(requester, product, amount);
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();

            entityManager.persist(order);
            entityManager.getTransaction().commit();

            entityManagerFactory.close();

            out.println("These are the order details:");
            out.println("\n");
            out.println("Product: " + product);
            out.println("\n");
            out.println("Amount: " + amount);
            out.println("\n");
            out.println("Requested By: " + requester);
            out.println("\n");
            out.println("Password: " + password);
            out.println("\n");
            out.println("</body></html>");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }
}
