import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "Orders")
public class Orders implements Serializable {
    public Orders(){

    }

    public Orders(String requester, String product_Name, int product_Amount) {
        Requester = requester;
        Product_Name = product_Name;
        Product_Amount = product_Amount;
    }

    @Id
    @Column(name = "idOrders")
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    private int idOrders;
    @Column(name = "Requester")
    private String Requester;
    @Column(name = "Product_Name")
    private String Product_Name;
    @Column(name = "Product_Amount")
    private int Product_Amount;

    public String getRequester() {
        return Requester;
    }

    public void setRequestor(String requester) {
        Requester = requester;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public int getProduct_Amount() {
        return Product_Amount;
    }

    public void setProduct_Amount(int product_Amount) {
        Product_Amount = product_Amount;
    }




    public int getIdOrders() {
        return idOrders;
    }

    public void setIdOrders(int idOrders) {
        this.idOrders = idOrders;
    }



}
