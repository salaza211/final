import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Inventory")
public class Inventory implements Serializable {
    @Id
    @Column(name = "idInventory")
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    private int idInventory;
    @Column(name = "product_name")
    private String product_name;
    @Column(name = "stock")
    private int stock;
    @Column(name = "Price")
    private int Price;

    public int getIdInventory() {
        return idInventory;
    }

    public void setIdInventory(int idInventory) {
        this.idInventory = idInventory;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public Inventory(){

    }

    public Inventory(String product_name, int stock, int price) {
        this.product_name = product_name;
        this.stock = stock;
        Price = price;
    }




}
